const createProgressRaw = (nickname, max, value, container) => {
  const progress = document.createElement('progress');
  progress.id = nickname;
  progress.max = max;
  progress.value = value;

  const nicknameEl = document.createElement('span');
  nicknameEl.innerHTML = nickname;

  container.append(nicknameEl, progress);
}
 
const refreshProgressBars = (map, maxValue, container) => {
  container.innerHTML = '';
  map.forEach((value, nickname) => {
    createProgressRaw(nickname, maxValue, value, container);
  })
}

window.onload = () => {

  const jwt = localStorage.getItem('jwt');

  if (!jwt) {
    location.replace('/login');
  } else {

    // partial application
    const refreshCurrentProgress = (map, maxValue) => {
      const progressContainer = document.querySelector('#progress-container');
      return refreshProgressBars(map, maxValue, progressContainer);
    }

    let curIndex = 0;
    let needNewText = true;
    let lvlChars, lvlCharsLength;
    const textEl = document.querySelector('#text');

    const fetchText = async () => {
      let lvlContent;
      await fetch('/level', {
        method: 'GET',
        headers: {
          'Authorization': 'Bearer ' + jwt,
        }
      })
      .then(res => res.json())
      .then(res => lvlContent = res.level);
      
      lvlChars = lvlContent.split('');
      lvlCharsLength = lvlChars.length;
      needNewText = false;

      curIndex = 0;

      Array.from(document.getElementsByTagName('progress')).map(el => {
        el.value = 0;
      });

      textEl.innerHTML = null;
      lvlChars.map((char, index) => {
        const el = document.createElement('div');
        el.classList.add('char');
        el.id = index;
        el.innerHTML = char === ' ' ? '&nbsp;' : char;
        textEl.append(el);
      });
    }
    
    const socket = io.connect('http://localhost:8000');

    socket.on('connect', async () => {
      const nickname = localStorage.getItem('nickname');
      socket.emit('room', { nickname });
      await fetchText();

      document.getElementById('commentator').style.display = 'block';

      document.addEventListener('keydown', event => {
        const char = event.key;
        if (lvlChars[curIndex] === char) {
          document.getElementById(curIndex).classList.add('complete');
          curIndex++;
          socket.emit('reload', { nickname, value: curIndex });
        }
      });

      window.onbeforeunload = () => {
        socket.emit('close', { nickname });
      }
    })

    socket.on('progressReload', ({ map }) => {
      let roomMap = new Map(JSON.parse(map));
      refreshCurrentProgress(roomMap, lvlCharsLength);
    })

    socket.on('commentator', message => {
      if(message) {
        console.log(message);
        document.getElementById('comment').textContent = message;
      }
    })

    socket.on('timer', async ({ status, duration }) => {
      const timerContainer = document.getElementsByClassName('timer-container')[0];
      const timeContainer = document.getElementById('time');
      const textContainer = document.getElementById('text');
      const mainContainer = document.querySelector('main');
      const textAfterTimerContainer = document.getElementById('text-after-timer');
      const textBeforeTimerContainer = document.getElementById('text-before-timer');
      const minutes = parseInt(duration / 60, 10);
      const seconds = parseInt(duration % 60, 10);

      timeContainer.innerHTML = `${minutes}m ${seconds}s`;

      if (status === 'wait') {
        needNewText = true;
        return;
      }

      if (status === 'pause') {
        textAfterTimerContainer.style.display = 'none';
        textBeforeTimerContainer.style.display = 'block';
        mainContainer.style.display = 'block';
        timerContainer.style.transform = 'translate(50%, 50%)';
        timerContainer.style.bottom = '50%';
        timerContainer.style.right = '50%';
        textEl.style.display = 'none';
        needNewText = true;
        return;
      }

      if (needNewText) {
        await fetchText();
      }

      textEl.style.display = 'flex';
      textBeforeTimerContainer.style.display = 'none';
      textAfterTimerContainer.style.display = 'block';
      mainContainer.style.display = 'block';
      timerContainer.style.transform = 'unset';
      timerContainer.style.bottom = '30px';
      timerContainer.style.right = '30px';
    })
  }
}
